# F1

## Exercise

We will use [The Ergast Developer API](http://ergast.com/mrd/) to create a single page application that presents a list that shows the F1 world champions starting from 2005 until 2015. Clicking on an item shows the list of the winners for every race for the selected year. We also request to highlight the row when the winner has been the world champion in the same season.

You can adjust the UI how you see fit for the best result, but sticking to a single page application is mandatory. We would prefer if you will deliver the code using GitHub/BitBucket (make sure we can share it among multiple people).

### Ergast Developer API
You can find all the information necessary to complete the exercise consulting the following website: http://ergast.com/mrd/

## Result

This project is created by Eduard Bespalov with [Angular CLI version 8.1.0](https://github.com/angular/angular-cli). The Angular framework and tools are chosen as a mature development environment. They allow to create quickly corporate level web applications. The project uses typescript, @ngrx to manage an application state, Angular Material for UI, Karma/Jasmine for unit testing.

### Prerequisites

Install NodeJS, Node-sass("npm i -g node-sass"), checkout the code and run "npm install" to install required dependencies.

### Development server

Run 'ng serve' to build and serve the application. Navigate to http://localhost:4200/

### Build

Run 'ng lint' to analyse the source code.
Run 'ng build' to build the project. The build artifacts will be stored in the 'dist' folder.
Run 'ng build --prod' to get a deployable production build of the project.

### Running unit tests

Run 'ng test' to execute the unit tests.

### Running end-to-end tests

Run 'ng e2e' to execute end-to-end tests via [Protractor](http://www.protractortest.org/)
