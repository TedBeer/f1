import { browser, by, element } from 'protractor';

export class AppPage {
	navigateTo() {
		return browser.get(browser.baseUrl) as Promise<any>;
	}

	getChampionsListTitleText() {
		return element(by.css('.champions-list-title')).getText() as Promise<string>;
	}
}
