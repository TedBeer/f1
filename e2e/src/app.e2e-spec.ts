import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('F1 App', () => {
	let page: AppPage;

	beforeEach(() => {
		page = new AppPage();
	});

	describe('list of champions', () => {
		it('should have a title', () => {
			page.navigateTo();
			expect(page.getChampionsListTitleText()).toEqual('F1 CHAMPIONS');
		});
	});

	afterEach(async () => {
		// Assert that there are no errors emitted from the browser
		const logs = await browser.manage().logs().get(logging.Type.BROWSER);
		expect(logs).not.toContain(jasmine.objectContaining({
			level: logging.Level.SEVERE,
		} as logging.Entry));
	});
});
