import { NgModule } from '@angular/core';
import {
	MatListModule, MatSnackBarModule, MatTooltipModule,
	MatTableModule, MatSortModule, MatButtonModule
} from '@angular/material';

@NgModule({
	declarations: [],
	imports: [
		MatListModule,
		MatSnackBarModule,
		MatTooltipModule,
		MatTableModule,
		MatSortModule,
		MatButtonModule
	],
	exports: [
		MatListModule,
		MatSnackBarModule,
		MatTooltipModule,
		MatTableModule,
		MatSortModule,
		MatButtonModule
	]
})

export class MaterialModule { }
