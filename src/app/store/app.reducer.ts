import {
	ActionReducer,
	ActionReducerMap,
	createFeatureSelector,
	createSelector,
	MetaReducer
} from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';
import * as fromDataReducer from './data.reducer';

export interface RootState {
	main: fromDataReducer.State;
}

export const reducers: ActionReducerMap<RootState> = {
	main: fromDataReducer.reducer
};

export const metaReducers: MetaReducer<RootState>[] =
	!environment.production ? [storeFreeze] : [];

const getMainState = (state: RootState) => state.main;

export const getChampions = createSelector(getMainState,
	(state: fromDataReducer.State) => state.champions);
export const getRaceWinners = createSelector(getMainState,
	(state: fromDataReducer.State) => state.raceWinners);
export const getSelectedSeason = createSelector(getMainState,
	(state: fromDataReducer.State) => state.selectedSeason);
export const getError = createSelector(getMainState,
	(state: fromDataReducer.State) => state.err);
export const getLoading = createSelector(getMainState,
	(state: fromDataReducer.State) => state.loading);
