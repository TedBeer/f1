import * as models from '@app/models';

export enum ActionTypes {
	SAVE_CHAMPION = 'Loads champion data from backend',
	SAVE_RACE_WINNERS = 'Loads race winners from backend',
	SELECT_SEASON = 'Selects a season to show all race winners',
	SET_LOADING = 'Sets the app loading state',
	SET_ERROR = 'Sets the app error state',
	CLEAR_ERROR = 'Resets the app error state',
}

export class SaveChampionAction {
	public type = ActionTypes.SAVE_CHAMPION;

	constructor( public payload: models.Champion) {}
}

export class SaveRaceWinnersAction {
	public type = ActionTypes.SAVE_RACE_WINNERS;

	constructor( public payload: Array<models.RaceWinner>) {}
}

export class SelectSeasonAction {
	public type = ActionTypes.SELECT_SEASON;

	constructor( public payload: models.SeasonId) {}
}

export class SetLoadingAction {
	public type = ActionTypes.SET_LOADING;

	constructor( public payload: boolean) {}
}

export class SetErrorAction {
	public type = ActionTypes.SET_ERROR;

	constructor( public payload: string) {}
}

export class ClearErrorAction {
	public type = ActionTypes.CLEAR_ERROR;

	constructor() {}
}
