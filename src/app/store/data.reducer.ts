import { createSelector } from '@ngrx/store';
import { sortBy, find, uniq } from 'lodash';
import { ActionTypes } from './data.actions';
import { SeasonId, Champion, RaceWinner } from '@app/models';

type Races = Map<SeasonId, Array<RaceWinner>>;

export interface State {
	loading: boolean;
	err: string | null;
	champions: Array<Champion>;
	raceWinners: Array<RaceWinner>;
	selectedSeason: SeasonId;
}

export const initialState: State = {
	loading: false,
	err: null,
	champions: [],
	raceWinners: [],
	selectedSeason: 0
};

export function reducer(state = initialState, action: any) {
	if (action && action.type) {
		switch (action.type) {
			case ActionTypes.SAVE_CHAMPION: {
				if (action.payload) {
					return {...state,
						champions: sortBy([...state.champions, action.payload], 'season')
					};
				}
				break;
			}
			case ActionTypes.SAVE_RACE_WINNERS: {
				return {...state,
					raceWinners: sortBy([...state.raceWinners, ...action.payload], ['season', 'round'])
				};
			}
			case ActionTypes.SELECT_SEASON:
				return {...state, selectedSeason: action.payload };

			case ActionTypes.SET_LOADING:
				return {...state, loading: action.payload };

			case ActionTypes.SET_ERROR:
				return {...state, error: action.payload };

			case ActionTypes.CLEAR_ERROR:
				return {...state, error: null };
		}
	}
	return state;
}
