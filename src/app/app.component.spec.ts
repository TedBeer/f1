import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule,
	HttpTestingController} from '@angular/common/http/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of as ObservableOf } from 'rxjs';
import { MaterialModule } from '@app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { BackendService } from '@app/services';
import { initialState } from '@app/store';
import { DATA_API_URL } from './config';
import { ChampionsComponent } from './components/champions/champions.component';
import { RaceWinnersComponent } from './components/race-winners/race-winners.component';

describe('AppComponent', () => {
	let app;
	let fixture;
	let backendSpy;

	beforeEach(async(() => {
		backendSpy = {
			loadRaceWinners: jasmine.createSpy('loadRaceWinners')
				.and.returnValue(ObservableOf([])),
			loadSeasonChampion: jasmine.createSpy('loadSeasonChampion')
				.and.returnValue(ObservableOf(null)),
			loadChampions: jasmine.createSpy('loadChampions')
				.and.returnValue(ObservableOf(null))
		};

		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule, NoopAnimationsModule, MaterialModule ],
			declarations: [
				RaceWinnersComponent,
				ChampionsComponent,
				AppComponent
			],
			providers: [
				{ provide: BackendService, useValue: backendSpy},
				{ provide: DATA_API_URL, useValue: 'http://test.com/f1/' },
				provideMockStore({ initialState: { main: initialState } })
			]
		}).compileComponents();
		fixture = TestBed.createComponent(AppComponent);
		app = fixture.debugElement.componentInstance;
	}));

	it('should create the app', () => {
		expect(app).toBeTruthy();
	});

	it('should load champions', () => {
		fixture.detectChanges();
		expect(backendSpy.loadChampions).toHaveBeenCalled();
	});
});
