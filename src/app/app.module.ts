import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from '@app/app.component';
import { StoreModule } from '@ngrx/store';
import { MaterialModule } from '@app/material.module';
import { reducers, metaReducers } from './store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { DATA_API_URL } from '@app/config';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChampionsComponent } from './components/champions/champions.component';
import { RaceWinnersComponent } from './components/race-winners/race-winners.component';

@NgModule({
	declarations: [
		AppComponent,
		ChampionsComponent,
		RaceWinnersComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		MaterialModule,
		StoreModule.forRoot(reducers, {
			metaReducers,
			runtimeChecks: {
				strictStateImmutability: true,
				strictActionImmutability: true
			}
		}),
		StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
		BrowserAnimationsModule
	],
	providers: [
		{ provide: DATA_API_URL, useValue: DATA_API_URL },
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
