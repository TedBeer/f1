import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '@app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ChampionsComponent } from './champions.component';
import * as testData from '@app/models/test-data';

describe('ChampionsComponent', () => {
	let component: ChampionsComponent;
	let fixture: ComponentFixture<ChampionsComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ NoopAnimationsModule, MaterialModule],
			declarations: [ ChampionsComponent ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ChampionsComponent);
		component = fixture.componentInstance;
	});

	it('should create', () => {
		fixture.detectChanges();
		expect(component).toBeTruthy();
	});

	it('should render list of champions', () => {
		component.list = testData.champions;
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		const names = compiled.querySelectorAll('.champions-list-item-full-name');
		expect(names.length).toBe(2);
		expect(names[0].textContent).toContain('Fernando Alonso');
		expect(names[1].textContent).toContain('Kimi Räikkönen');
	});
});
