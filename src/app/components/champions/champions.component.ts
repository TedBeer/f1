import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as models from '@app/models';

@Component({
	selector: 'f1-champions',
	templateUrl: './champions.component.html',
	styleUrls: ['./champions.component.scss']
})
export class ChampionsComponent implements OnInit {
	@Input() list: Array<models.Champion> = [];
	@Input() season: models.SeasonId = 0;
	@Output() seasonChanged = new EventEmitter<models.SeasonId>();

	constructor() { }

	ngOnInit() {
	}
}
