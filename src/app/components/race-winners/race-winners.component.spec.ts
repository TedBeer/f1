import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from '@app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { RaceWinnersComponent } from './race-winners.component';
import * as testData from '@app/models/test-data';

describe('RaceWinnersComponent', () => {
	let component: RaceWinnersComponent;
	let fixture: ComponentFixture<RaceWinnersComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [ NoopAnimationsModule, MaterialModule],
			declarations: [ RaceWinnersComponent ]
		})
		.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(RaceWinnersComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should render a title', () => {
		component.champion = testData.champions[0];
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;

		const title = compiled.querySelector('.winners-list-title');
		expect(title.textContent).toContain('2005');
	});

	it('should render list of race winners', () => {
		component.list = testData.raceWinners;
		component.champion = testData.champions[0];
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;

		const names = compiled.querySelectorAll('.winners-list-name');
		expect(names.length).toBe(4);
		expect(names[0].textContent).toContain('Giancarlo Fisichella');
		expect(names[1].textContent).toContain('Fernando Alonso');
	});

	it('should highlight the champion in the list', () => {
		component.list = testData.raceWinners;
		component.champion = testData.champions[0];
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;

		const names = compiled.querySelectorAll('.highlighted .winners-list-name');
		expect(names.length).toBe(3);
		expect(names[0].textContent).toContain('Fernando Alonso');
	});
});
