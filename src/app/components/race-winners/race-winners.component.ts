import { Component, Input } from '@angular/core';
import { Sort } from '@angular/material';
import * as models from '@app/models';
import { at as _at, sortBy as _sortBy } from 'lodash';

const dirSort = (a, b, dir) => {
	const res = a > b ? 1 : (a < b ? -1 : 0);
	return dir === 'asc' ? res : res * (-1);
};

@Component({
	selector: 'f1-race-winners',
	templateUrl: './race-winners.component.html',
	styleUrls: ['./race-winners.component.scss']
})

export class RaceWinnersComponent {
	@Input()
		set list(value: Array<models.RaceWinner>) {
			this.sortedList = value;
			this.sortData(this.sort);
		}
	@Input() champion: models.Champion;

	public displayedColumns = ['round', 'name', 'race', 'time'];
	public sortedList: Array<models.RaceWinner> = [];
	private sort: Sort = {active: 'round', direction: 'asc'};

	constructor() { }

	isChampion(winner: models.RaceWinner) {
		return this.champion ?
			_at(this.champion, 'Driver.driverId')[0] === winner.Driver.driverId : false;
	}

	sortData(sort: Sort) {
		this.sort = sort;
		switch (sort.active) {
			case 'name':
				this.sortedList = this.sortedList.slice().sort(
					(a, b) => dirSort(a.fullName, b.fullName, sort.direction)
				);
				break;
			case 'round':
				this.sortedList = this.sortedList.slice().sort(
					(a, b) => dirSort(a.round, b.round, sort.direction)
				);
				break;
			case 'race':
				this.sortedList = this.sortedList.slice().sort(
					(a, b) => dirSort(a.Race.raceName, b.Race.raceName, sort.direction)
				);
				break;
		}
	}
}
