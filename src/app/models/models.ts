import * as data from './data';

export interface Champion extends data.DriverStanding {
	season: data.SeasonId;
	fullName: string;
}

export interface RaceWinner extends data.RaceResult {
	season: data.SeasonId;
	round: number;
	Race: data.Race;
	fullName: string;
}
