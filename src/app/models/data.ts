export type SeasonId = number;

export interface Driver {
	driverId: string;
	givenName: string;
	familyName: string;
	dateOfBirth: string;
	nationality: string;
	url?: string;
}

export interface Constructor {
	constructorId: string;
	name: string;
	nationality: string;
	url?: string;
}

export interface DriverStanding {
	position: string;
	points: string;
	wins: string;
	Driver: Driver;
	Constructors: Array<Constructor>;
}

export interface RaceResult {
	points: string;
	Driver: Driver;
	Constructor: Constructor;
	laps: string;
	status: string;
	Time: {
		time: string;
	};
}

export interface Race {
	season: string;
	round: string;
	url: string;
	raceName: string;
	date: string;
	Results: Array<RaceResult>;
}
