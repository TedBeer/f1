import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject, combineLatest } from 'rxjs';
import { auditTime, takeUntil } from 'rxjs/operators';
import { findIndex as _findIndex, find as _find } from 'lodash';
import { MatSnackBar } from '@angular/material';
import {
	trigger, state, style, animate, transition
} from '@angular/animations';
import { BackendService } from '@app/services';
import { START_SEASONS } from './config';
import * as fromStore from '@app/store';
import * as models from '@app/models';

@Component({
	selector: 'f1-root',
	templateUrl: './app.component.html',
	animations: [
		trigger('refreshUpdated', [
			state('refresh', style({
				transform: 'rotateY(90deg)'
			})),
			state('updated', style({
				transform: 'rotateY(0deg)'
			})),
			transition('refresh <=> updated', [
				animate('0.5s')
			]),
		]),
	],
	styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
	title = 'F1 champions';
	years = [];
	champions$: Observable<Array<models.Champion>>;
	champions: Array<models.Champion> = [];
	raceWinners$: Observable<Array<models.RaceWinner>>;
	raceWinners: Array<models.RaceWinner> = [];
	selectedSeason$: Observable<models.SeasonId>;
	season: models.SeasonId = 0;
	seasonRaceWinners: Array<models.RaceWinner> = [];
	seasonState = 'refresh';
	error$: Observable<string>;
	error: string;
	loading$: Observable<boolean>;
	loading = false;
	seasonChampion: models.Champion = null;

	private snakBarRef: any;
	// keep requested years to retry in a case of failure
	private requestedYears: Array<number> = [];
	private destroyed$ = new Subject<boolean>();

	constructor(
			private snackBar: MatSnackBar,
			public backendService: BackendService,
			public store: Store<fromStore.RootState>) {

		this.champions$ = this.store.select(fromStore.getChampions);
		this.raceWinners$ = this.store.select(fromStore.getRaceWinners);
		this.selectedSeason$ = this.store.select(fromStore.getSelectedSeason);
		this.error$ = this.store.select(fromStore.getError);
		this.loading$ = this.store.select(fromStore.getLoading);
	}

	ngOnInit() {
		this.error$.pipe(takeUntil(this.destroyed$)).subscribe(err => {
			this.error = err;
			if (err) {
				this.showError(err);
			} else if (this.snakBarRef) {
				this.snakBarRef.dismiss();
			}
		});
		this.loading$.pipe(takeUntil(this.destroyed$))
			.subscribe(value => this.loading = value);

		this.selectedSeason$.pipe(takeUntil(this.destroyed$)).subscribe(season => {
			this.season = season;
			this.seasonChampion = this.findChampion(season);
			if (season > 0) {
				this.seasonState = 'refresh';
				if (this.isRaceWinnersAvailable(season)) {
					setTimeout(() => {
						this.seasonRaceWinners = this.getSeasonRaceWinners(season, this.raceWinners);
						this.seasonState = 'updated';
					}, 500);
				} else {
					this.loadRaceWinners(season);
				}
			}
		});
		this.champions$.pipe(takeUntil(this.destroyed$), auditTime(200))
			.subscribe(list => this.champions = list);
		this.raceWinners$.pipe(takeUntil(this.destroyed$)).subscribe(list => {
			this.raceWinners = list;
			setTimeout(() => {
				this.seasonState = 'updated';
				this.seasonRaceWinners = this.getSeasonRaceWinners(this.season, list);
			}, 500);
		});
		this.load(START_SEASONS);
	}

	ngOnDestroy() {
		this.destroyed$.next(true);
		this.destroyed$.complete();
	}

	load(years: Array<number>) {
		this.requestedYears = years;
		this.store.dispatch(new fromStore.SetLoadingAction(true));
		this.backendService.loadChampions(...years).subscribe(
			champion => {
				if (champion) {
					this.store.dispatch(new fromStore.SaveChampionAction(champion));
					this.years.push(champion.season);
				}
			},
			err => this.store.dispatch(new fromStore.SetErrorAction(err)),
			() => {
				this.store.dispatch(new fromStore.SetLoadingAction(false));
				this.requestedYears = [];
			}
		);
	}

	loadRaceWinners(season: models.SeasonId) {
		this.store.dispatch(new fromStore.SetLoadingAction(true));
		this.backendService.loadRaceWinners(season).subscribe(
			winners => this.store.dispatch(
					new fromStore.SaveRaceWinnersAction(winners)),
			err => this.store.dispatch(new fromStore.SetErrorAction(err)),
			() => this.store.dispatch(new fromStore.SetLoadingAction(false))
		);
	}

	showError(err) {
		this.snakBarRef = this.snackBar.open(err, 'reload', {
			panelClass: 'snackbar-error'
		});
		this.snakBarRef.onAction()
			.subscribe(() => this.load(this.requestedYears));
	}

	onSeasonChanged(season: models.SeasonId) {
		this.store.dispatch(new fromStore.SelectSeasonAction(season));
	}

	isRaceWinnersAvailable(season: models.SeasonId): boolean {
		return _findIndex(this.raceWinners, { season }) >= 0;
	}

	findChampion(season: models.SeasonId): models.Champion {
		return _find(this.champions, { season });
	}

	getSeasonRaceWinners(
				season: models.SeasonId,
				list: Array<models.RaceWinner>): Array<models.RaceWinner> {
		if (season && list.length > 0) {
			const winners = list.filter( winner => winner.season === season);
			return winners;
		}
		return [];
	}

	moreYearsAvailable(): boolean {
		const lastLoaded = Math.max(...this.years);
		return (new Date()).getFullYear() !== lastLoaded;
	}

	loadMore() {
		const lastLoaded = Math.max(...this.years);
		const thisYear = (new Date()).getFullYear();
		if (thisYear !== lastLoaded) {
			const arr = [];
			for (let i = lastLoaded + 1; i <= thisYear; i++) {
				arr.push(i);
			}
			this.load(arr);
		}
	}
}
