import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable, merge } from 'rxjs';
import { map as rxjsMap } from 'rxjs/operators';
import { at as _at, map as _map } from 'lodash';
import { DATA_API_URL } from '../config';

import * as models from '@app/models';

@Injectable({
	providedIn: 'root'
})

export class BackendService {

	constructor(private http: HttpClient, @Inject(DATA_API_URL) private apiUrl) { }

	loadRaceWinners(season: models.SeasonId): Observable<Array<models.RaceWinner>> {
		const url = `${this.apiUrl}${season}/results/1.json`;
		return this.http
			.get(url, {responseType: 'json'})
			.pipe(rxjsMap(response => {
				const races = _at(response, 'MRData.RaceTable.Races')[0] || [];
				return _map(races, race => this.getRaceWinner(race));
			}));
	}

	loadSeasonChampion(season: models.SeasonId): Observable<models.Champion> {
		const url = `${this.apiUrl}${season}/driverStandings/1.json`;
		return this.http
			.get(url, {responseType: 'json'})
			.pipe(rxjsMap(response => this.getChampion(
				season,
				_at(response, 'MRData.StandingsTable.StandingsLists[0].DriverStandings[0]')[0]
			)));
	}

	loadChampions(...seasons): Observable<models.Champion> {
		return merge(
			..._map(seasons, season => this.loadSeasonChampion(season))
		);
	}

	// TODO: a separate data transformation service
	getChampion(season: models.SeasonId, driverStanding: models.DriverStanding): models.Champion {
		const driver = {givenName: '', familyName: '', ..._at(driverStanding, 'Driver')[0]};
		return {
			...driverStanding,
			season,
			fullName: `${driver.givenName} ${driver.familyName}`
		};
	}

	getRaceWinner(race: models.Race): models.RaceWinner {
		const topResult = race.Results[0];
		const driver = {givenName: '', familyName: '', ..._at(topResult, 'Driver')[0]};
		return {
			...topResult,
			season: parseInt(race.season, 10),
			round: parseInt(race.round, 10),
			fullName: `${driver.givenName} ${driver.familyName}`,
			Race: race
		};
	}
}
