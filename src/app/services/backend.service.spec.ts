import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { at, uniq } from 'lodash';
import { BackendService } from './backend.service';
import * as testData from '@app/models/test-data';
import { DATA_API_URL } from '../config';


describe('BackendService', () => {
	const dataUrl = 'http://test.com/f1/';
	let httpClient: HttpClient;
	let httpTestingController: HttpTestingController;
	let service: BackendService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [HttpClientTestingModule],
			providers: [
				{ provide: DATA_API_URL, useValue: dataUrl },
				BackendService
			]
		});
		httpClient = TestBed.get(HttpClient);
		httpTestingController = TestBed.get(HttpTestingController);
		service = TestBed.get(BackendService);
	});

	afterEach(() => {
		httpTestingController.verify();
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});

	it('should load world champion 2008', (done) => {
			service.loadSeasonChampion(2008).subscribe(champion => {
				expect(at(champion, 'Driver.driverId')[0]).toBe('hamilton');
				done();
			});

			const req = httpTestingController.expectOne(`${dataUrl}2008/driverStandings/1.json`);

			expect(req.request.method).toEqual('GET');
			req.flush(testData.season2008Champion);
	});

	it('should load world champions 2008 and 2011', (done) => {
			const champions = [];
			service.loadChampions(2008, 2011).subscribe(champion => {
				champions.push(at(champion, 'Driver.driverId')[0]);
			},
			err => console.log(err),
			() => {
				expect(champions.length).toBe(2);
				expect(champions.indexOf('vettel') >= 0).toBeTruthy();
				expect(champions.indexOf('hamilton') >= 0).toBeTruthy();
				done();
			});

			const requests = httpTestingController.match(
				req => /(2008|2011)\/driverStandings\/1\.json$/.test(req.url)
			);
			expect(requests.length).toEqual(2);

			requests[0].flush(testData.season2008Champion);
			requests[1].flush(testData.season2011Champion);
	});

	it('should load season 2008 races', (done) => {
			service.loadRaceWinners(2008).subscribe(winners => {
				expect(winners.length).toBe(18);
				const list = uniq(
						winners.map(winner => at(winner, 'Driver.driverId')[0])
					).sort();
				expect(list).toEqual(['alonso', 'hamilton', 'kovalainen', 'kubica', 'massa', 'raikkonen', 'vettel']);
				done();
			});

			const req = httpTestingController.expectOne(`${dataUrl}2008/results/1.json`);
			expect(req.request.method).toEqual('GET');
			req.flush(testData.season2008Races);
	});
});
